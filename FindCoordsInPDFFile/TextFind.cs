﻿using System.Collections.Generic;
using iTextSharp.text.pdf.parser;

namespace FindCoordsInPDFFile
{
    public class LocationTextExtractionStrategyWithPosition : LocationTextExtractionStrategy
    {
        private readonly List<TextChunk> _locationalResult = new List<TextChunk>();

        private readonly ITextChunkLocationStrategy _tclStrat;

        public LocationTextExtractionStrategyWithPosition() : this(new TextChunkLocationStrategyDefaultImp())
        {
        }

        /**
         * Creates a new text extraction renderer, with a custom strategy for
         * creating new TextChunkLocation objects based on the input of the
         * TextRenderInfo.
         * @param strat the custom strategy
         */
        private LocationTextExtractionStrategyWithPosition(ITextChunkLocationStrategy strat)
        {
            _tclStrat = strat;
        }


        private static bool StartsWithSpace(string str)
        {
            if (str.Length == 0) return false;
            return str[0] == ' ';
        }


        private static bool EndsWithSpace(string str)
        {
            if (str.Length == 0) return false;
            return str[^1] == ' ';
        }

        /**
         * Filters the provided list with the provided filter
         * @param textChunks a list of all TextChunks that this strategy found during processing
         * @param filter the filter to apply.  If null, filtering will be skipped.
         * @return the filtered list
         * @since 5.3.3
         */
        private static List<TextChunk> FilterTextChunks(List<TextChunk> textChunks, ITextChunkFilter filter)
        {
            if (filter == null)
            {
                return textChunks;
            }

            var filtered = new List<TextChunk>();

            foreach (var textChunk in textChunks)
            {
                if (filter.Accept(textChunk))
                {
                    filtered.Add(textChunk);
                }
            }

            return filtered;
        }

        public override void RenderText(TextRenderInfo renderInfo)
        {
            var segment = renderInfo.GetBaseline();
            if (renderInfo.GetRise() != 0)
            {
                var riseOffsetTransform = new Matrix(0, -renderInfo.GetRise());
                segment = segment.TransformBy(riseOffsetTransform);
            }
            var tc = new TextChunk(renderInfo.GetText(), _tclStrat.CreateLocation(renderInfo, segment));
            _locationalResult.Add(tc);
        }


        public IEnumerable<TextLocation> GetLocations()
        {
            var filteredTextChunks = FilterTextChunks(_locationalResult, null);
            filteredTextChunks.Sort();

            TextChunk lastChunk = null;

            var textLocations = new List<TextLocation>();

            foreach (var chunk in filteredTextChunks)
            {

                if (lastChunk == null)
                {
                    // Инициализация
                    textLocations.Add(new TextLocation
                    {
                        Text = chunk.Text,
                        X = iTextSharp.text.Utilities.PointsToMillimeters(chunk.Location.StartLocation[0]),
                        Y = iTextSharp.text.Utilities.PointsToMillimeters(chunk.Location.StartLocation[1])
                    });

                }
                else
                {
                    if (chunk.SameLine(lastChunk))
                    {
                        var text = "";
                        // Вставляем пробел только в том случае, если конечный символ предыдущей строки не был пробелом, а начальный символ текущей строки не является пробелом
                        if (IsChunkAtWordBoundary(chunk, lastChunk) && !StartsWithSpace(chunk.Text) && !EndsWithSpace(lastChunk.Text))
                            text += ' ';
                        text += chunk.Text;
                        textLocations[^1].Text += text;
                    }
                    else
                    {
                        textLocations.Add(new TextLocation
                        {
                            Text = chunk.Text,
                            X = iTextSharp.text.Utilities.PointsToMillimeters(chunk.Location.StartLocation[0]),
                            Y = iTextSharp.text.Utilities.PointsToMillimeters(chunk.Location.StartLocation[1])
                        });
                    }
                }
                lastChunk = chunk;
            }
            // Возвращаем местоположения с заданными текстами
            return textLocations;
        }
    }

    public class TextLocation
    {
        public string Text { get; set; }
        public float X { get; set; }
        public float Y { get; set; }
        
    }
}