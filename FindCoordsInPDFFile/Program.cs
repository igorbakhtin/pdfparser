﻿using System;
using System.Linq;
using iTextSharp.text.pdf;
using iTextSharp.text.pdf.parser;

namespace FindCoordsInPDFFile
{
    public static class Program
    {
        private static void Main(string[] args)
        {
            const string inputPdf = @"C:\pdf\f.pdf";
            const string requiredSymbol = "©";
            using var reader = new PdfReader(inputPdf);
            var parser = new PdfReaderContentParser(reader);
            var numberOfPages = reader.NumberOfPages;
            for (var i = 1; i <= numberOfPages; i++)
            {
                var strategy = parser.ProcessContent(i, new LocationTextExtractionStrategyWithPosition());
                var res = strategy.GetLocations();
                    
                var searchResults = res.Where(p => p.Text.Contains(requiredSymbol)).OrderBy(p => p.Y).Reverse().ToList();
                foreach (var searchResult in searchResults)
                {
                    Console.WriteLine("Номер страницы: {0}; X коорд.: {1}; Y коорд.: {2}", i, searchResult.X, searchResult.Y);
                }
            }
            reader.Close();
        }
    }
}
